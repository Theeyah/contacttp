const express = require('express');
const app = express();
const cookieParser = require("cookie-parser");
const path = require("path");
const contactRouter = require('./route/contactRouter');
const registerContact = require('./route/registerContactRouter');
const contactViewDetail = require('./route/contactViewDetailRouter');
const connectDB = require("./models/connexionDB");
const contactService = require("./services/contactService");
const { resolve } = require('path');
const { rejects } = require('assert');
const bodyParser = require('body-parser');

// la database doit déja être créer ./models/connexionDB pour avoir les infos (pour le créer automatiquement même principe que la table mais sur une database)
connectDB.getConnection();


//ejs
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser());
app.use(express.static(path.join(__dirname, '/public')));

//Vérification du Middleware
app.use("/", contactRouter);
app.use("/", registerContact);
app.use("/", contactViewDetail);

app.listen(3080, function () {
    console.log('Example app listening on port 3080!');
});

module.exports = app;