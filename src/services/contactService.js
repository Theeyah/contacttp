const connectDB = require("../models/connexionDB");

function getListContact(){
    return connectDB.query("SELECT name FROM contact");
}

function getUniqueContact(id){
  return connectDB.query("SELECT contact.name FROM contact WHERE contact.id in ('"+id+"')");
}

function getUniqueContactDetail(id){
    return connectDB.query("SELECT * FROM contact WHERE contact.id="+id+"");
}

function postRegisterUniqueContact(name, job, tel, mail){
    return connectDB.query("INSERT INTO contact (name, job, tel, mail) VALUES ('"+name+"', '"+job+"', '"+tel+"', '"+mail+"')");
}

function deleteUniqueContact(id){
    return connectDB.query("DELETE FROM contact WHERE id="+id+";");
}

function putUniqueContactName(parameterModify, id){
    return connectDB.query("UPDATE contact SET name="+parameterModify+" WHERE id="+id+";");
}
function putUniqueContactJob(parameterModify, id){
    return connectDB.query("UPDATE contact SET job="+parameterModify+" WHERE id="+id+";");
}
function putUniqueContactTel(parameterModify, id){
    return connectDB.query("UPDATE contact SET tel="+parameterModify+" WHERE id="+id+";");
}
function putUniqueContactMail(parameterModify, id){
    return connectDB.query("UPDATE contact SET mail="+parameterModify+" WHERE id="+id+";");
}
// function putUniqueContact(nameParameter, parameterModify, id){
//     return connectDB.query("UPDATE contact SET "+nameParameter+"="+parameterModify+" WHERE id="+id+";");
// }
function putUniqueContact(name, job, tel, mail, id){
    return connectDB.query("UPDATE contact SET name='"+name+"', job='"+job+"', tel='"+tel+"', mail='"+mail+"' WHERE id="+id+";");
}

function setID(name,job,tel,mail){
    return connectDB.query("SELECT id FROM contact WHERE name="+name+" AND job="+job+" AND tel="+tel+" AND mail="+mail+";");
}

function setID(){
    return connectDB.query("SELECT contact.id FROM contact");
}


module.exports = { getListContact, getUniqueContact, getUniqueContactDetail, postRegisterUniqueContact, deleteUniqueContact, putUniqueContactName, putUniqueContactJob, putUniqueContactMail, putUniqueContact, setID };
