const mariadb = require('mariadb');

const connexion = mariadb.createPool({ host: "localhost", database: "db_contact" ,user: "adminContact", password: "adminadmin", connectionLimit: 5 });

connexion.getConnection()
    .then(conn => {
      console.log('1');
      // si la table n'éxiste peut on peut pas accèder à l'étape suivant donc go to catch et creation de table sinon on est connecté
      conn.query("SELECT * FROM contact")
      //inutile 2 et 3
        .then(rows => { 
          console.log('2');
          // return conn.query("CREATE TABLE IF NOT EXISTS contact(id int(11) auto_increment NOT NULL, name varchar(255) DEFAULT NULL,job varchar(255) DEFAULT NULL,tel int(11) DEFAULT NULL,mail varchar(255) DEFAULT NULL, PRIMARY KEY (id))", [1, "mariadb"])
          // rows: [ {val: 1}, meta: ... ]
        })
        .then(res => { // res: { affectedRows: 1, insertId: 1, warningStatus: 0 }
          console.log('3');
          // console.log(res);
          // conn.release(); // release to pool
        })
        .catch(err => {
          console.log('4');
          return conn.query("CREATE TABLE IF NOT EXISTS contact(id int(11) auto_increment NOT NULL, name varchar(255) DEFAULT NULL,job varchar(255) DEFAULT NULL,tel varchar(10) DEFAULT NULL,mail varchar(255) DEFAULT NULL, PRIMARY KEY (id))", [1, "mariadb"])
          conn.release(); // release to pool
        })
        
    }).catch(err => {
      console.log('5');
      //not connected
    });
module.exports = connexion;