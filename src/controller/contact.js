const contactService = require("../services/contactService");
// const connectDB = require("../models/connexionDB");
const bodyParser = require('body-parser');


//traitement de route
//
const listContactRouter = function (req, res, next) {
        const list = [];
        contactService.getListContact().then((row) => {
        row.forEach(element => 
        {
            list.push(element.name);
        });   
        res.render("home", { listContact: list });  
    });
}

const deleteContactRouter = function(req, res, next){
    console.log(req.query.id);
    contactService.deleteUniqueContact(req.query.id)
    .then((value) => {
        const list = [];
        contactService.getListContact().then((row) => {
            row.forEach(element => 
            {
                list.push(element.name);
            });   
            res.render("home", { listContact: list });  
        });
    })
}

const getRegisterContactRouter = function (req, res) {
    res.render("registerContact");
}

const setRegisterContact = function (req, res) {
    const name = req.body.Name
    // const name = req.id.name;
    const job = req.body.Job;
    const tel = req.body.Tel;
    const mail = req.body.Mail;
    contactService.postRegisterUniqueContact(name, job, tel, mail);
    // console.log('ok')
    res.render("registerContact");
}


const contactDetail = function (req, res, next) {
    const list = [];
    const id = req.params.id;
    contactService.getUniqueContactDetail(id).then((row) => {
        row.forEach(element => {
            // console.log(req.params.id);
            return list.push(element.name, element.job, element.tel, element.mail, element.id);
        });
        console.log(list);
        res.render("contactDetail", 
        { 
            contactName: list[0],
            contactJob: list[1],
            contactTel: list[2],
            contactMail: list[3],
            contactId: list[4]
        });
    })
}

const contactDetailUpdate = function (req, res, next) {
    // console.log(req.body.id);
    contactService.putUniqueContact(req.body.name,req.body.job,req.body.tel,req.body.mail,req.body.id);
    const list = [];
    contactService.getListContact().then((row) => {
        row.forEach(element => 
        {
            list.push(element.name);
        });   
        res.render("home", { listContact: list });  
    });
}

module.exports = {
  listContactRouter,
  deleteContactRouter,
  getRegisterContactRouter,
  setRegisterContact,
  contactDetail,
  contactDetailUpdate
};