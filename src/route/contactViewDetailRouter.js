const express = require('express');
const router = express.Router();
const contact = require("../controller/contact");


router.get("/contactUniqueDetail/:id", contact.contactDetail, (req, res) =>{
});

router.post("/contactUniqueDetailPut", contact.contactDetailUpdate);

router.get("/contactUniqueDetailDelete", contact.deleteContactRouter);

module.exports = router;