const express = require('express');
const router = express.Router();
const contactService = require('../services/contactService');
const contact = require('../controller/contact');

//router - rediriger les bons middlewares

router.get("/", contact.listContactRouter);

// router.delete("/", contact.deleteContactRouter);


module.exports = router;