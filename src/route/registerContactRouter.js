const express = require('express');
const router = express.Router();
const contact = require("../controller/contact");

router.get("/register", contact.getRegisterContactRouter, (req, res) => {
});

router.post("/registerPost", contact.setRegisterContact, (req, res) => {
});
// ok
module.exports = router;